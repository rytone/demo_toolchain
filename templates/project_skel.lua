-------------
-- metadata--
-------------
name("{{ name  }}")
author("{{ author }}")
-- OPTIONAL: 
--    music("path/to/music")
--    music_bpm(120)


------------
-- events --
------------
function init()
	-- example
	--    vertex_shader("nop", "nop.vs")
	--    fragment_shader("raymarch", "raymarch.fs")
	--    shader_pair("main", "nop", "raymarch")
end

function draw()
	-- example
	--    clear_color(0, 0, 0)
	--
	--    set_shaders("main")
	--    draw_mesh("quad")
end
