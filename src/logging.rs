use ansi_term::Style;
use ansi_term::Colour::{Yellow, Red};

pub struct Logger {
    pub verbose: bool,
}

impl Logger {
    pub fn debug(&self, msg: &str) {
        if self.verbose {
            println!("{}", msg);
        }
    }

    pub fn info(&self, msg: &str) {
        println!("{}", Style::new().bold().paint(format!(" :: {}", msg)));
    }

    pub fn important_info(&self, msg: &str) {
        println!("{}", Style::new().bold().paint(format!(" >>> {}", msg)));
    }

    pub fn warn(&self, msg: &str) {
        println!("{}", Yellow.bold().paint(format!(" *** {}", msg)));
    }

    pub fn error(&self, msg: &str) {
        println!("{}", Red.bold().paint(format!(" !!! {}", msg)));
    }
}
