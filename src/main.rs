mod logging;
mod init;
mod debug;
mod project_config;
mod event;
mod state;
mod datatypes;

extern crate ansi_term;
extern crate clap;
#[macro_use]
extern crate glium;
extern crate hlua;
extern crate liquid;
extern crate stopwatch;

use logging::Logger;
use init::init;
use debug::debug;

use clap::{Arg, App, SubCommand};

fn main() {
    let args = App::new("demo_toolchain")
        .version("1.0")
        .author("rytone <rytonemail@gmail.com>")
        .about("toolchain for creating size-restricted demos")
        .arg(Arg::with_name("verbose")
                 .short("v")
                 .long("verbose")
                 .help("sets the log level to something stupid"))
        .arg(Arg::with_name("templates")
                 .short("t")
                 .long("templates")
                 .value_name("PATH")
                 .help("path to templates")
                 .takes_value(true))
        .subcommand(SubCommand::with_name("init").about("create a new project"))
        .subcommand(SubCommand::with_name("debug")
            .about("run a project without building it")
            .arg(Arg::with_name("stepthrough")
                .short("s")
                .long("stepthrough")
                .help("lets you step through events one by one")))
        .get_matches();

    let verbose = args.is_present("verbose");
    let log = Logger { verbose: verbose };
    log.debug("logging verbosely");

    let exe_loc = std::env::current_exe().expect("failed to get executable location!");
    let template_path = args.value_of("templates")
        .unwrap_or(exe_loc.parent().unwrap().to_str().unwrap());
    log.debug(format!("reading templates from {}", template_path).as_str());

    if args.subcommand_matches("init").is_some() {
        init(&log, template_path);
    }

    match args.subcommand_matches("debug") {
        Some(args) => debug(&log, args.is_present("stepthrough")),
        _ => {},
    }
}
