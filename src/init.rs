use std::fs::File;
use std::io;
use std::io::Read;
use std::io::Write;
use std::path::Path;

use logging::Logger;
use project_config::write_project;

use liquid;
use liquid::{Renderable, Context, Value};

fn query_parameter(name: &str) -> Result<String, &str> {
    let mut param = String::new();

    print!("{}: ", name);
    io::stdout().flush().unwrap();
    let res = io::stdin().read_line(&mut param);
    if res.is_err() {
        return Err("error reading input");
    }
    let trimmed = param.as_str().trim();
    if trimmed.is_empty() {
        return Err("cannot be empty");
    }

    return Ok(trimmed.to_string());
}

fn read_template(templates: &str) -> String {
    let templates_path = Path::new(templates).join("project_skel.lua");
    let mut template_file = File::open(templates_path).expect("could not read project template!");
    let mut template = String::new();
    template_file
        .read_to_string(&mut template)
        .expect("could not read template into string!");

    return template;
}

fn render(templates: &str, name: &str, author: &str) -> String {
    let template = read_template(templates);

    let renderer = liquid::parse(template.as_str(), Default::default()).unwrap();

    let mut context = Context::new();
    context.set_val("name", Value::Str(name.to_string()));
    context.set_val("author", Value::Str(author.to_string()));

    return renderer.render(&mut context).unwrap().unwrap();
}

pub fn init(log: &Logger, templates: &str) {
    log.important_info("creating new project...");

    let mut name = String::new();
    let mut author = String::new();

    match query_parameter("name") {
        Ok(n) => name = n,
        Err(e) => log.error(format!("error reading name: {}", e).as_str()),
    }
    match query_parameter("author") {
        Ok(a) => author = a,
        Err(e) => log.error(format!("error reading author: {}", e).as_str()),
    }

    log.info("writing project config...");

    log.debug("rendering...");
    let output = render(templates, name.as_str(), author.as_str());

    log.debug(format!("writing project:\n{:?}", output).as_str());
    write_project(output);

    log.important_info("done!");
}
