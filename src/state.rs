use std::collections::HashMap;

use datatypes::Vertex;

use glium::{VertexBuffer, IndexBuffer};
use glium::program::Program;

use stopwatch::Stopwatch;

pub struct DebugDemoState {
    pub shaders: HashMap<String, Program>,
    pub vertex_bufs: HashMap<String, VertexBuffer<Vertex>>,
    pub index_bufs: HashMap<String, IndexBuffer<u16>>,

    pub active_shaders: String,

    pub time: Stopwatch,
    pub resolution: [f32; 2],
}

impl DebugDemoState {
    pub fn new() -> DebugDemoState {
        return DebugDemoState {
                   shaders: HashMap::new(),
                   vertex_bufs: HashMap::new(),
                   index_bufs: HashMap::new(),

                   active_shaders: String::new(),

                   time: Stopwatch::new(),
                   resolution: [1280.0, 720.0], // TODO ples no hardcode
               };
    }
}
