use std::cell::RefCell;
use std::fs::File;
use std::io;
use std::io::Read;
use std::io::Write;

use event;
use event::DemoEvent;

use hlua;
use hlua::{Lua, LuaError};

pub fn read_project() -> Result<String, io::Error> {
    let mut proj = String::new();
    match File::open("./project.lua") {
        Ok(mut f) => {
            f.read_to_string(&mut proj)
                .expect("failed to read project into string!");
            return Ok(proj);
        }
        Err(e) => return Err(e),
    }
}

pub fn write_project(body: String) {
    let mut file = File::create("project.lua").expect("could not open project file");
    file.write_all(body.into_bytes().as_slice())
        .expect("could not write project file");
}

pub struct Project {
    pub name: String,
    pub author: String,

    pub init_events: Vec<Box<DemoEvent>>,
    pub draw_events: Vec<Box<DemoEvent>>,
}

impl Project {
    pub fn new(source: String) -> Result<Project, LuaError> {
        let mut name = String::new();
        let mut author = String::new();

        let init_events: Vec<Box<DemoEvent>> = Vec::new();
        let init_ev_ref = RefCell::new(init_events);

        let draw_events: Vec<Box<DemoEvent>> = Vec::new();
        let draw_ev_ref = RefCell::new(draw_events);

        {
            let mut lua = Lua::new();
            lua.set("name", hlua::function1(|n: String| name = n));
            lua.set("author", hlua::function1(|a: String| author = a));

            let lua_res = lua.execute::<()>(source.as_str());
            if lua_res.is_err() {
                return Err(lua_res.err().unwrap());
            }

            lua.set("debug_test",
                    hlua::function1(|s: String| {
                                        let evt = event::DebugTestEvent { message: s };
                                        init_ev_ref.borrow_mut().push(Box::new(evt));
                                    }));

            lua.set("shader_pair",
                    hlua::function3(|n: String, v: String, f: String| {
                                        let evt = event::ShaderEvent {
                                            name: n,
                                            vs_path: v,
                                            fs_path: f,
                                        };
                                        init_ev_ref.borrow_mut().push(Box::new(evt));
                                    }));


            let lua_res = lua.execute::<()>("init()");
            if lua_res.is_err() {
                return Err(lua_res.err().unwrap());
            }

            lua.set("clear_color",
                    hlua::function3(|r: f32, g: f32, b: f32| {
                                        let evt = event::ClearColorEvent { r: r, g: g, b: b };
                                        draw_ev_ref.borrow_mut().push(Box::new(evt));
                                    }));

            lua.set("set_shaders",
                    hlua::function1(|n: String| {
                                        let evt = event::SetShaderEvent { name: n };
                                        draw_ev_ref.borrow_mut().push(Box::new(evt));
                                    }));

            lua.set("draw_mesh",
                    hlua::function1(|n: String| {
                                        let evt = event::DrawEvent { name: n };
                                        draw_ev_ref.borrow_mut().push(Box::new(evt));
                                    }));

            let lua_res = lua.execute::<()>("draw()");
            if lua_res.is_err() {
                return Err(lua_res.err().unwrap());
            }
        }

        return Ok(Project {
                      name: name,
                      author: author,

                      init_events: init_ev_ref.into_inner(),
                      draw_events: draw_ev_ref.into_inner(),
                  });
    }
}
