use std::io;

use logging::Logger;
use project_config::{Project, read_project};
use state::DebugDemoState;
use datatypes::Vertex;

use glium;
use glium::index::PrimitiveType;
use glium::DisplayBuild;
use glium::backend::Facade;

fn run_demo(project: &Project, log: &Logger, st: bool) {
    if st {
        log.warn("stepthrough enabled... press enter to advance events.");
    }

    log.debug("creating context...");
    let display = glium::glutin::WindowBuilder::new()
        .with_dimensions(1280, 720)
        .with_title(format!("{} - {}", project.author, project.name))
        .with_srgb(Some(false))
        .build_glium()
        .unwrap();

    log.debug(format!("supported opengl version: {:?}",
                      display.get_context().get_opengl_version())
                      .as_str());

    log.debug("initializing state");
    let mut state = DebugDemoState::new();

    log.debug("initializing default vertex/index buffers");
    state
        .vertex_bufs
        .insert("quad".to_string(),
                glium::VertexBuffer::new(&display,
                                         &[Vertex { position: [-1.0, 1.0, 0.0] },
                                           Vertex { position: [1.0, 1.0, 0.0] },
                                           Vertex { position: [1.0, -1.0, 0.0] },
                                           Vertex { position: [-1.0, -1.0, 0.0] }])
                        .unwrap());
    state
        .index_bufs
        .insert("quad".to_string(),
                glium::IndexBuffer::new(&display,
                                        PrimitiveType::TrianglesList,
                                        &[0 as u16, 1, 2, 2, 3, 0])
                        .unwrap());

    {
        log.debug("running init events");
        let mut frame = display.draw();
        for event in project.init_events.iter() {
            match event.debug(&display, &mut frame, &mut state) {
                Err(e) => log.warn(format!("{} ... continuing anyway.", e).as_str()),
                _ => {},
            };
            if st {
                io::stdin().read_line(&mut String::new()).unwrap();
            }
        }
        frame.finish().unwrap();
    }

    log.debug("starting stopwatch");
    state.time.start();

    log.debug("entering draw loop");
    loop {
        let mut target = display.draw();

        for event in project.draw_events.iter() {
            match event.debug(&display, &mut target, &mut state) {
                Err(e) => log.warn(format!("{} ... continuing anyway.", e).as_str()),
                _ => {},
            };
            if st {
                io::stdin().read_line(&mut String::new()).unwrap();
            }
        }

        target.finish().unwrap();

        for ev in display.poll_events() {
            match ev {
                glium::glutin::Event::Closed => return,
                _ => (),
            }
        }
    }
}

pub fn debug(log: &Logger, st: bool) {
    log.important_info("debugging project...");

    log.info("reading config...");
    match read_project() {
        Ok(p) => {
            log.debug("loading config...");
            match Project::new(p) {
                Ok(project) => {
                    log.debug("no errors encountered.");
                    if project.name.is_empty() {
                        log.warn("project name is empty... continuing anyway.")
                    }
                    if project.author.is_empty() {
                        log.warn("project author is empty... continuing anyway.")
                    }
                    log.info("starting...");
                    run_demo(&project, log, st);
                    log.info("done!");
                }
                Err(e) => {
                    log.error(format!("could not load project config: {:?}", e).as_str());
                }
            }
        }
        Err(e) => {
            log.error(format!("could not read project config: {}", e).as_str());
        }
    }
}
