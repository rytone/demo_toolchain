use std::fs::File;
use std::io::Read;

use state::DebugDemoState;

use glium;
use glium::Surface;
use glium::backend::glutin_backend::GlutinFacade;

pub trait DemoEvent {
    fn debug(&self, display: &GlutinFacade, frame: &mut glium::Frame, state: &mut DebugDemoState) -> Result<(), String>;
    fn generate(&self) -> String;
}



pub struct DebugTestEvent {
    pub message: String,
}

impl DemoEvent for DebugTestEvent {
    fn debug(&self, _: &GlutinFacade, _: &mut glium::Frame, _: &mut DebugDemoState) -> Result<(), String> {
        println!(" -- {}", self.message);
        return Ok(());
    }

    fn generate(&self) -> String {
        return String::new();
    }
}



pub struct ShaderEvent {
    pub name: String,
    pub vs_path: String,
    pub fs_path: String,
}

impl ShaderEvent {
    fn load_vs(&self) -> Result<String, String> {
        let mut f = match File::open(self.vs_path.clone()) {
            Err(_) => return Err(format!("failed to open vertex shader {}", self.vs_path)),
            Ok(f) => f,
        };
        let mut source = String::new();
        match f.read_to_string(&mut source) {
            Err(_) =>  return Err(format!("failed to read vertex shader {}", self.vs_path)),
            _ => {},
        };
        return Ok(source);
    }

    fn load_fs(&self) -> Result<String, String> {
        let mut f = match File::open(self.fs_path.clone()) {
            Err(_) => return Err(format!("failed to open fragment shader {}", self.fs_path)),
            Ok(f) => f,
        };
        let mut source = String::new();
        match f.read_to_string(&mut source) {
            Err(_) =>  return Err(format!("failed to read fragment shader {}", self.fs_path)),
            _ => {},
        };
        return Ok(source);
    }
}

impl DemoEvent for ShaderEvent {
    fn debug(&self, display: &GlutinFacade, _: &mut glium::Frame, state: &mut DebugDemoState) -> Result<(), String> {
        let vs_source = match self.load_vs() {
            Err(e) => return Err(e),
            Ok(s) => s,
        };
        let fs_source = match self.load_fs() {
            Err(e) => return Err(e),
            Ok(s) => s,
        };

        let program = match glium::Program::from_source(display, vs_source.as_str(), fs_source.as_str(), None) {
            Err(e) => return Err(format!("error compiling shaders: {:?}", e)),
            Ok(p) => p,
        };

        state.shaders.insert(self.name.clone(), program);

        return Ok(());
    }

    fn generate(&self) -> String {
        return String::new(); // TODO implement
    }
}



pub struct ClearColorEvent {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

impl DemoEvent for ClearColorEvent {
    fn debug(&self, _: &GlutinFacade, frame: &mut glium::Frame, _: &mut DebugDemoState) -> Result<(), String> {
        frame.clear_color(self.r, self.g, self.b, 1.0);
        return Ok(());
    }

    fn generate(&self) -> String {
        return String::new(); // TODO implement
    }
}



pub struct SetShaderEvent {
    pub name: String,
}

impl DemoEvent for SetShaderEvent {
    fn debug(&self, _: &GlutinFacade, _: &mut glium::Frame, state: &mut DebugDemoState) -> Result<(), String> {
        state.active_shaders = self.name.clone();
        return Ok(());
    }

    fn generate(&self) -> String {
        return String::new(); // TODO implement
    }
}



pub struct DrawEvent {
    pub name: String,
}

impl DemoEvent for DrawEvent {
    fn debug(&self, _: &GlutinFacade, frame: &mut glium::Frame, state: &mut DebugDemoState) -> Result<(), String> {
        let vb = match state.vertex_bufs.get(&self.name) {
            None => return Err(format!("could not fetch vertex buffer from state for {}", self.name)),
            Some(b) => b,
        };
        let ib = match state.index_bufs.get(&self.name) {
            None => return Err(format!("could not fetch index buffer from state for {}", self.name)),
            Some(b) => b,
        };

        let shader = match state.shaders.get(&state.active_shaders) {
            None => return Err(format!("could not fetch shader from state for {}", state.active_shaders)),
            Some(s) => s,
        };

        let uniforms = uniform! {
            time: state.time.elapsed_ms() as f32 / 1000.0,
            resolution: state.resolution,
        };

        match frame.draw(vb, ib, shader, &uniforms, &Default::default()) {
            Err(e) => return Err(format!("error compiling shaders: {:?}", e)),
            _ => {},
        };

        return Ok(());
    }

    fn generate(&self) -> String {
        return String::new(); // TODO implement
    }
}
