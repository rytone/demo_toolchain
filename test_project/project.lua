-------------
-- metadata--
-------------
name("test")
author("rytone")
-- OPTIONAL: 
--    music("path/to/music")
--    music_bpm(120)


------------
-- events --
------------
function init()
	debug_test("this a test event!")
	shader_pair("main", "nop.vs", "raymarch.fs")
end

function draw()
	clear_color(0, 0, 0)
	
	set_shaders("main")
	draw_mesh("quad")
end
