#version 330

#define EPSILON 0.001
#define FAR 32.0
#define MAX_STEPS 64
#define PI 3.14159265359
#define DEG_TO_RAD PI / 180.0

out vec4 color;

uniform float time;
uniform vec2 resolution;

float prim_sphere(vec3 p, float s) {
    return length(p) - s;
}

float prim_box(vec3 p, vec3 b) {
  vec3 d = abs(p) - b;
  return min(max(d.x,max(d.y,d.z)),0.0) + length(max(d,0.0));
}

float map(vec3 p) {   
    p = mod(p, 4.0) - 2.0;
    
    float center_box = prim_box(p, vec3(0.5, 0.5, 0.5));
    float cross_1 = prim_box(p, vec3(2.0, 0.1, 0.1));
    float cross_2 = prim_box(p, vec3(0.1, 2.0, 0.1));
    float cross_3 = prim_box(p, vec3(0.1, 0.1, 2.0));
    
    float final = FAR;
    final = min(final, center_box);
    final = min(final, cross_1);
    final = min(final, cross_2);
    final = min(final, cross_3);
    
    return final;
}

float march_map(vec3 eye, vec3 dir) {
    float depth = 0.0;
    vec3 pos = eye;
    
    for (int i = 0; i < MAX_STEPS; i++) {
        float d = map(pos);
        depth += d;
        pos += dir * d;
        
        if (d < EPSILON) {
            break;
        }
        
        if (d >= FAR) {
            return FAR;
        }
    }
    
    return depth;
}

vec3 map_grad(vec3 p) {
    return normalize(vec3(
        map(vec3(p.x + EPSILON, p.y, p.z)) - map(vec3(p.x - EPSILON, p.y, p.z)),
        map(vec3(p.x, p.y + EPSILON, p.z)) - map(vec3(p.x, p.y - EPSILON, p.z)),
        map(vec3(p.x, p.y, p.z  + EPSILON)) - map(vec3(p.x, p.y, p.z - EPSILON))
    ));
}

vec3 dir_from_fov(float fov, vec2 pos, vec2 res) {
	vec2 xy = pos - res * 0.5;

	float cot_half_fov = tan((90.0 - fov * 0.5) * DEG_TO_RAD);	
	float z = res.y * 0.5 * cot_half_fov;
	
	return normalize( vec3( xy, -z ) );
}

mat3 look_at(vec3 eye, vec3 center, vec3 up) {
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    return mat3(s, u, -f);
}

vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main() {
    
    
    ///////////////////////////
    
    vec3 eye = vec3(sin(time) * 5.0, -time * 8.0, sin(time * 1.4) * 5.0);
    vec3 look = vec3(sin(time/5.0), sin(time)*(FAR/10.0) - 4.0, cos(time/5.0));
    vec3 light = eye + vec3(sin(time * 3.0) * 5.0, -10.0, cos(time * 2.4) * 8.0);
    
    vec3 bg = vec3(0.1, 0.1, 0.1);
    vec3 ambient = vec3(0.05, 0.05, 0.05);
    
    vec3 light_color = vec3(1.0, 1.0, 1.0);
    
    ///////////////////////////
    
    
    look = eye + look;
    vec3 dir = dir_from_fov(80.0, gl_FragCoord.xy, resolution);
    mat3 view = look_at(eye, look, vec3(0, 1, 0));
    dir = view * dir;
    
    float depth = march_map(eye, dir);
    if (depth >= FAR) {
        color = vec4(bg, 1.0);
    } else {
        vec3 hit_pos = eye + dir * depth;
        vec3 nrm = map_grad(hit_pos);
        vec3 to_light = normalize(light - hit_pos);
        
        float light_dist = 1.0 - min(length(light - hit_pos) / 25.0, 1.0);
        float light_amnt = dot(nrm, to_light) * light_dist;
        
        float v =  1.0 - depth / FAR;
        
        
        ///////////////////////////
        
        vec3 surf_color = hsv2rgb(vec3(v / 2.5 + 0.6, 0.5, 0.9));
        
        ///////////////////////////
        
        
        surf_color = mix(surf_color, light_color, light_amnt);
        surf_color = mix(ambient, surf_color, light_dist);
        surf_color = mix(bg, surf_color, v);
        
        color = vec4(surf_color, 1.0);
    }
}